import React, { Component} from 'react';
import axios from 'axios';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import {Button} from "@material-ui/core";

/*We create a variable that stores the information on the book we want to add/Modify/Delete and all the data from the database to displauy it*/
export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      title:'',
      author:'',
      category:'',
      data:[]
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.deleteBook = this.deleteBook.bind(this);
    this.handleGet = this.handleGet.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleGetBook = this.handleGetBook.bind(this);
    this.getBooks = this.getBooks.bind(this);
    this.getBooks()
  }

/*this function allows the state to be update each time the value in the text box is modified allowing us to use it directly when we hit one of the submit button */
  handleChange(event) {
    const inputValue = event.target.value;
    const stateField = event.target.name;
    this.setState({[stateField]: inputValue,});
    console.log(this.state);
  }

/*The function that puts the book in the db*/
  async handleSubmit(event) {
    event.preventDefault();
    const { id,title,author,category } = this.state;
    await axios.put(
      'https://jcwuowqbml.execute-api.us-east-2.amazonaws.com/books',
      { id: `${id}`,title:`${title}`,author:`${author}`,category:`${category}` }
    ); 
    this.handleGet(event);
  }

/*The functions that delete a book in the db, might be useful if the database is large*/
  async handleDelete(event) {
    event.preventDefault();
    const id = this.state.id;
    await axios.delete('https://jcwuowqbml.execute-api.us-east-2.amazonaws.com/books/'+id,);    
    this.handleGet(event);
  }
async deleteBook(id) {
  await axios.delete('https://jcwuowqbml.execute-api.us-east-2.amazonaws.com/books/'+id,);  
  this.getBooks();
  }

/*The function that get the books in the db*/
/*Get All books*/
  async handleGet(event) {
    event.preventDefault();
    this.getBooks();}
/*Get a book might be useful if the database is large*/
  async handleGetBook(event) {
      event.preventDefault();
      const id = this.state.id;
      const data = await axios.get('https://jcwuowqbml.execute-api.us-east-2.amazonaws.com/books/'+id).then(res => res.data);
      this.setState({data: [data.Item]});}
  async getBooks() {
    const data = await axios.get('https://jcwuowqbml.execute-api.us-east-2.amazonaws.com/books').then(res => res.data);
    this.setState({data: data.Items});}
    
/*The return that displays everything*/
/*First the differents zone text and buttons then the databse */
  render() {
    return (
      <div>
        <h1>
          <form onSubmit={this.handleSubmit}>
            <label>id:</label>
            <input
              type="text"
              name="id"
              onChange={this.handleChange}
              value={this.state.id}/>
            <label>title:</label>
            <input
              type="text"
              name="title"
              onChange={this.handleChange}
              value={this.state.title}/>
            <label>author:</label>
            <input
              type="text"
              name="author"
              onChange={this.handleChange}
              value={this.state.author}/>
            <label>category:</label>
            <input
              type="text"
              name="category"
              onChange={this.handleChange}
              value={this.state.category}/>
            <button type="submit">Add/Modify</button>
         </form>
        </h1>
        <h2>
          <form onSubmit={this.handleDelete}>
            <label>id:</label>
            <input
              type="text"
              name="id"
              onChange={this.handleChange}
              value={this.state.id}/>
              <button type="submit">Delete</button></form>
        </h2>
        <h3>
          <form onSubmit={this.handleGetBook}>
            <label>id:</label>
            <input
              type="text"
              name="id"
              onChange={this.handleChange}
              value={this.state.id}/>
              <button type="submit">Get Book</button>
              </form>
        </h3>
        <h4>
        <form onSubmit={this.handleGet}>
            <button type="submit">Actualiser</button>
            </form>
        </h4>
        <h5>
          <TableContainer >
             <Table>
               <TableHead>
                 <TableRow >
                   <TableCell align="center">Id</TableCell>
                   <TableCell align="center">Title</TableCell>
                   <TableCell align="center">Author</TableCell>
                   <TableCell align="center">Category</TableCell>
                   <TableCell align="center">Delete</TableCell>
                 </TableRow>
                 </TableHead>
                 <TableBody>
                   {this.state.data.map((item) => (
                                <TableRow key={item.id}>
                                  <TableCell component="th" scope={"row"}>
                                  {item.id}
                                  </TableCell>
                                  <TableCell align={"center"}>{item.title}</TableCell>
                                  <TableCell align={"center"}>{item.author}</TableCell>
                                  <TableCell align={"center"}>{item.category}</TableCell>
                                  <TableCell align={"center"}>
                                  <Button onClick={() => this.deleteBook(item.id)}>
                                  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOY6t7523b2O6G1Z9mHEfhLNKcrbE2-FgTDw&usqp=CAU" alt=""/>
                                  </Button>
                                  </TableCell>
                                </TableRow>
                                    ))}
                  </TableBody>
               </Table>
            </TableContainer>
       </h5>
    </div>
   );
 }
}